package yabu.inatel.br.nicechat.utils

import android.content.Context
import android.content.SharedPreferences

/**
 * Preference class to store user nickname.
 */
class Prefs(context: Context) {

    companion object {
        val PREFS_FILENAME = "yabu.techchat"
        val USER_NAME = "user_name"
    }

    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    var userName: String? = null
        get() = prefs.getString(USER_NAME, field)
        set(value) = prefs.edit().putString(USER_NAME, value).apply()
}