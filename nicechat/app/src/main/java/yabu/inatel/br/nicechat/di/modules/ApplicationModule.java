package yabu.inatel.br.nicechat.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import yabu.inatel.br.nicechat.ChatApplication;

/**
 * Module to provide Application.
 */
@Module
public class ApplicationModule {

    private final ChatApplication mApplication;

    public ApplicationModule(ChatApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication.getApplicationContext();
    }
}
