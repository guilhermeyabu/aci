package yabu.inatel.br.nicechat.ui.splashscreen;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.ChatApplication;
import yabu.inatel.br.nicechat.data.model.Topic;
import yabu.inatel.br.nicechat.ui.AppController;
import yabu.inatel.br.nicechat.ui.nickname.NicknameActivity;
import yabu.inatel.br.nicechat.ui.techlist.TechListActivity;
import yabu.inatel.br.nicechat.utils.Constants;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * {@link SplashScreenActivity} ViewModel that handle operations to the correspondent Activity.
 */
public class SplashScreenViewModel {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * Activity's Lifecycle.
   */
  private LifecycleOwner mLifecycleOwner;

  @Inject
  Context mContext;

  /**
   * Controller to handle {@link android.arch.persistence.room.RoomDatabase} operations.
   */
  @Inject
  AppController appController;

  @Inject
  public SplashScreenViewModel() {
  }


  /**
   * Set {@link SplashScreenActivity} lifecycle to observe {@link android.arch.lifecycle.LiveData}.
   *
   * @param lifecycleOwner - Activity Lifecycle.
   */
  void setLifecycleOwner(LifecycleOwner lifecycleOwner) {
    mLifecycleOwner = lifecycleOwner;
  }

  /**
   * Checks if user has already insert a nickname.
   *
   * @param context - Activity context.
   */
  void checkUserRegistration(Context context) {
    if (ChatApplication.prefs.getUserName() == null) {
      if (Logger.DEBUG) Log.d(TAG, "user not registered");
      NicknameActivity.startActivity(context);
    } else {
      if (Logger.DEBUG) Log.d(TAG, "user already registered");
      TechListActivity.startActivity(context);
    }
  }

  /**
   * If is the first time in the app, initialize the topics in
   * {@link android.arch.persistence.room.RoomDatabase}.
   */
  void initializeTopics() {
    if (Logger.DEBUG) Log.d(TAG, "initializeTopics");
    appController.getTopics().observe(mLifecycleOwner, topicsList -> {
      if (topicsList == null || topicsList.size() <= 0) {
        Topic topic;
        for (String topicTitle : Constants.topicList) {
          topic = new Topic();
          topic.setIsSubscribed(false);
          topic.setTopicName(topicTitle);
          appController.createNewTopic(topic);
        }
      }
    });
  }
}
