package yabu.inatel.br.nicechat.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import yabu.inatel.br.nicechat.data.dao.TopicDao;
import yabu.inatel.br.nicechat.data.model.Topic;

/**
 * Define database tables, version and dao's.
 */
@Database(entities = {Topic.class}, version = 4)
public abstract class ApplicationDatabase extends RoomDatabase {

    public abstract TopicDao topicDao();
}
