package yabu.inatel.br.nicechat.ui.techlist;

import android.databinding.BaseObservable;
import android.util.Log;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.data.model.Topic;
import yabu.inatel.br.nicechat.ui.AppController;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * ViewModel to control each Topic in the app, updating when necessary.
 */
public class TechListRowViewModel extends BaseObservable {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * Controller to handle {@link android.arch.persistence.room.RoomDatabase} operations.
   */
  @Inject
  AppController appController;

  @Inject
  public TechListRowViewModel() {
  }

  /**
   * Updates a {@link Topic} in database
   *
   * @param topic - updates current topic in database.
   */
  void updateTopic(Topic topic) {
    if (Logger.DEBUG) Log.d(TAG, "updateTopic");
    appController.updateTopic(topic);
  }
}
