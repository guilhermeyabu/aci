package yabu.inatel.br.nicechat.ui.nickname;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import java.util.Objects;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.R;
import yabu.inatel.br.nicechat.databinding.ActivityNicknameBinding;
import yabu.inatel.br.nicechat.di.Injector;
import yabu.inatel.br.nicechat.ui.techlist.TechListActivity;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * Nickname Activity that define a nickname to the user.
 */
public class NicknameActivity extends AppCompatActivity {

  /**
   * Tag to control Logs.
   */
  public static final String TAG = Logger.getTag();

  /**
   * Binding variable to control layout operations.
   */
  private ActivityNicknameBinding mBinding;

  /**
   * ViewModel to handle operations.
   */
  @Inject
  public NicknameViewModel viewModel;

  /**
   * Starts the {@link NicknameActivity}.
   * @param context - context of the previous Activity.
   */
  public static void startActivity(Context context) {
    if (Logger.DEBUG) Log.d(TAG, "startActivity");
    Intent intent = new Intent(context, NicknameActivity.class)
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(intent);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (Logger.DEBUG) Log.d(TAG, "onCreate");

    Injector.getApplicationComponent().inject(this);

    mBinding = DataBindingUtil.setContentView(this, R.layout.activity_nickname);
    mBinding.setViewModel(viewModel);

    viewModel.getHasToCallTechListActivity().observe(this, hasToCall -> {
      if (Objects.requireNonNull(hasToCall)) {
        TechListActivity.startActivity(this);
        finish();
      }
    });
  }

  @Override
  protected void onResume() {
    if (Logger.DEBUG) Log.d(TAG, "onResume");

    super.onResume();
    viewModel.onResume();
    addTextListenerToEditText();
  }

  private void addTextListenerToEditText() {
    mBinding.etNickname.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //do nothing
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        //do nothing
      }

      @Override
      public void afterTextChanged(Editable text) {
        viewModel.verifyNickname(text.toString());
      }
    });
  }
}
