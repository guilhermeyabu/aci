package yabu.inatel.br.nicechat.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Helper class to check connectivity status.
 */
class ConnectionHelper {

    companion object {

        @JvmStatic
        fun hasNetworkData(context: Context): Boolean {
            val connMgr = context.getSystemService(
                    Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connMgr.activeNetworkInfo

            return networkInfo != null && networkInfo.isConnected
        }
    }
}
