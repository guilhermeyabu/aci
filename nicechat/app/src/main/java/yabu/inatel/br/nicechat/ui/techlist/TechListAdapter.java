package yabu.inatel.br.nicechat.ui.techlist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Provider;

import yabu.inatel.br.nicechat.R;
import yabu.inatel.br.nicechat.data.model.Topic;
import yabu.inatel.br.nicechat.databinding.CardTopicBinding;
import yabu.inatel.br.nicechat.ui.chatroom.ChatRoomActivity;
import yabu.inatel.br.nicechat.utils.ConnectionHelper;
import yabu.inatel.br.nicechat.utils.Constants;
import yabu.inatel.br.nicechat.utils.Logger;
import yabu.inatel.br.nicechat.utils.SingleLiveEvent;
import yabu.inatel.br.nicechat.utils.adapter.BindingHolder;
import yabu.inatel.br.nicechat.utils.adapter.GenericAdapter;

public class TechListAdapter extends GenericAdapter<Topic, BindingHolder<CardTopicBinding>> {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * {@link android.arch.lifecycle.LiveData} to indicates when
   * {@link yabu.inatel.br.nicechat.ui.chatroom.ChatRoomActivity} to be call with an intent.
   */
  private final SingleLiveEvent<Intent> intentToChat;

  /**
   * Provider to get a ViewModel to each {@link Topic} in the list.
   */
  @Inject
  Provider<TechListRowViewModel> mTechListRowViewModelProvider;

  /**
   * Context to get resources.
   */
  @Inject
  Context mContext;

  @Inject
  public TechListAdapter() {
    super();
    intentToChat = new SingleLiveEvent<>();
  }

  @NonNull
  @Override
  public BindingHolder<CardTopicBinding> onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    if (Logger.DEBUG) Log.d(TAG, "onCreateViewHolder");
    return new BindingHolder<>(mContext, parent, R.layout.card_topic);
  }

  @Override
  public void onBindViewHolder(
      @NonNull BindingHolder<CardTopicBinding> holder, int position) {
    if (Logger.DEBUG) Log.d(TAG, "onBindViewHolder");

    Topic topic = getItem(position);
    CardTopicBinding row = holder.binding;
    TechListRowViewModel viewModel = mTechListRowViewModelProvider.get();

    if (row != null) {
      setupTopic(row, viewModel, topic);
    }
  }

  private void setupTopic(CardTopicBinding row, TechListRowViewModel viewModel, Topic topic) {
    row.setViewModel(viewModel);
    row.tvTopicName.setText(topic.getTopicName());
    checkIfIsSubscribed(row, topic);
    setupSubscription(row, viewModel, topic);
  }

  private void checkIfIsSubscribed(CardTopicBinding row, Topic topic) {
    if (topic.isSubscribed()) {
      row.clUnsubscribe.setVisibility(View.VISIBLE);
      row.clMessage.setVisibility(View.VISIBLE);
      row.clSubscribe.setVisibility(View.GONE);
    } else {
      row.clUnsubscribe.setVisibility(View.GONE);
      row.clMessage.setVisibility(View.GONE);
      row.clSubscribe.setVisibility(View.VISIBLE);
    }
  }

  private void setupSubscription(CardTopicBinding row,
      TechListRowViewModel viewModel, Topic topic) {
    row.clSubscribe.setOnClickListener(v -> {
      topic.setIsSubscribed(true);
      viewModel.updateTopic(topic);
      notifyDataSetChanged();
    });

    row.clMessage.setOnClickListener(v -> {
      if (!ConnectionHelper.hasNetworkData(mContext)) {
        Toast.makeText(mContext, mContext.getString(R.string.no_network_error),
            Toast.LENGTH_LONG).show();
      } else {
        Intent intent = new Intent(mContext, ChatRoomActivity.class);
        intent.putExtra(Constants.ROOM_NAME_HEADER, topic.getTopicName());
        intentToChat.setValue(intent);
      }
    });

    row.clUnsubscribe.setOnClickListener(v -> {
      topic.setIsSubscribed(false);
      viewModel.updateTopic(topic);
      notifyDataSetChanged();
    });
  }

  SingleLiveEvent<Intent> getIntentToChat() {
    return intentToChat;
  }
}
