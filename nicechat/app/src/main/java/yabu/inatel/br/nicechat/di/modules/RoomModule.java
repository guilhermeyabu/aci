package yabu.inatel.br.nicechat.di.modules;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import yabu.inatel.br.nicechat.data.room.ApplicationDatabase;

/**
 * Module to define Room Database structure.
 */
@Module
public class RoomModule {

    private ApplicationDatabase mApplicationDatabase;

    public static final String DATABASE_NAME = "nicechat_dtabase";

    public RoomModule(Application mApplication) {
        this.mApplicationDatabase = Room.databaseBuilder(mApplication, ApplicationDatabase.class,
                DATABASE_NAME).fallbackToDestructiveMigration().build();
    }

    @Provides
    @Singleton
    ApplicationDatabase providesDb() {
        return mApplicationDatabase;
    }
}
