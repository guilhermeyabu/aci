package yabu.inatel.br.nicechat.di;

import java.util.Objects;

import yabu.inatel.br.nicechat.ChatApplication;
import yabu.inatel.br.nicechat.di.component.ApplicationComponent;
import yabu.inatel.br.nicechat.di.component.DaggerApplicationComponent;
import yabu.inatel.br.nicechat.di.modules.ApplicationModule;
import yabu.inatel.br.nicechat.di.modules.RoomModule;

/**
 * Injector class to inject dependencies in the app.
 */
public final class Injector {

  private static ApplicationComponent sApplicationComponent;

  public static void initializeApplicationComponent(ChatApplication app) {
    sApplicationComponent = DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(app))
        .roomModule(new RoomModule(app))
        .build();
  }

  public static ApplicationComponent getApplicationComponent() {
    Objects.requireNonNull(sApplicationComponent, "Null ApplicationComponent");
    return sApplicationComponent;
  }
}
