package yabu.inatel.br.nicechat.ui.chatroom;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import java.util.Objects;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.R;
import yabu.inatel.br.nicechat.databinding.ActivityChatRoomBinding;
import yabu.inatel.br.nicechat.di.Injector;
import yabu.inatel.br.nicechat.utils.Constants;
import yabu.inatel.br.nicechat.utils.LoadingDialogFragment;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * Chat Room Activity where users can send and read messages from a specific group.
 */
public class ChatRoomActivity extends AppCompatActivity {

  /**
   * Tag to control Logs.
   */
  public static final String TAG = Logger.getTag();

  /**
   * Binding variable to control layout operations.
   */
  private ActivityChatRoomBinding mBinding;

  /**
   * Shows Loading action.
   */
  @Inject
  LoadingDialogFragment loadingDialogFragment;

  /**
   * ViewModel to handle operations.
   */
  @Inject
  ChatRoomViewModel viewModel;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (Logger.DEBUG) Log.d(TAG, "onCreate");

    Injector.getApplicationComponent().inject(this);
    mBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat_room);
    mBinding.rvChat.setAdapter(viewModel.chatRoomAdapter);

    String roomName = Objects.requireNonNull(getIntent().getExtras()).getString(
        Constants.ROOM_NAME_HEADER);

    setTitle(roomName);

    viewModel.setRoomName(roomName);
    viewModel.setBinding(mBinding);

    mBinding.setViewModel(viewModel);

    addTextListenerToEditText();

    Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    viewModel.getLoadingHasFinished().observe(this, hasFinished -> {
      if (hasFinished) {
        loadingDialogFragment.dismiss();
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    viewModel.onResume();
    loadingDialogFragment.show(getSupportFragmentManager(), LoadingDialogFragment.TAG);
  }

  @Override
  public boolean onSupportNavigateUp() {
    if (Logger.DEBUG) Log.d(TAG, "onSupportNavigateUp");
    onBackPressed();
    return true;
  }

  private void addTextListenerToEditText() {
    mBinding.etNewMessage.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //do nothing
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        //do nothing
      }

      @Override
      public void afterTextChanged(Editable text) {
        viewModel.verifyMessage(text.toString());
      }
    });
  }
}
