package yabu.inatel.br.nicechat.ui.nickname;

import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;

import java.util.Objects;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.ChatApplication;
import yabu.inatel.br.nicechat.ui.AppController;
import yabu.inatel.br.nicechat.utils.Constants;
import yabu.inatel.br.nicechat.utils.Logger;
import yabu.inatel.br.nicechat.utils.SingleLiveEvent;

/**
 * {@link NicknameActivity} Viewmodel that handle operations to the correspondent Activity.
 */
public class NicknameViewModel {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * Store user nickname.
   */
  public final ObservableField<String> nickname;

  /**
   * Controls error label {@link android.widget.TextView} visibility.
   */
  public final ObservableField<Integer> errorLabelVisibility;

  /**
   * Controls the send button {@link android.widget.Button} clickable action.
   */
  public final ObservableField<Boolean> continueButtonIsClickable;

  /**
   * Controls the send button {@link android.widget.Button} visibility/alpha
   */
  public final ObservableField<Float> continueButtonVisibility;

  /**
   * {@link android.arch.lifecycle.LiveData} to indicates when
   * {@link yabu.inatel.br.nicechat.ui.techlist.TechListActivity} has to be call.
   */
  private final SingleLiveEvent<Boolean> hasToCallTechListActivity;

  /**
   * Controller to handle {@link android.arch.persistence.room.RoomDatabase} operations.
   */
  @Inject
  public AppController appController;

  @Inject
  public NicknameViewModel() {
    nickname = new ObservableField<>();
    errorLabelVisibility = new ObservableField<>(View.GONE);
    continueButtonIsClickable = new ObservableField<>(false);
    continueButtonVisibility = new ObservableField<>(Constants.DISABLE_ALPHA);
    hasToCallTechListActivity = new SingleLiveEvent<>();
  }

  /**
   * Reflects Activity onResume lifecycle.
   */
  void onResume() {
    if (Logger.DEBUG) Log.d(TAG, "onResume");
  }

  /**
   * Store nickname in {@link yabu.inatel.br.nicechat.utils.Prefs} and calls
   * {@link yabu.inatel.br.nicechat.ui.techlist.TechListActivity}.
   *
   * @return - new View.OnClickListener.
   */
  public View.OnClickListener continueClick() {
    return v -> {
      if (Logger.DEBUG) Log.d(TAG, "CREATING NEW USER");
      createAndSaveNickname();
      hasToCallTechListActivity.setValue(true);
    };
  }

  private void createAndSaveNickname() {
    ChatApplication.prefs.setUserName(Objects.requireNonNull(nickname.get()));
  }

  /**
   * Check if message that will be send to the chat is ok.
   *
   * @param nickname - user nickname to be saved.
   */
  void verifyNickname(String nickname) {
    if (nickname.length() > 2) {
      enableContinueButton();
    } else {
      disableContinueButton();
    }
  }

  private void enableContinueButton() {
    continueButtonVisibility.set(Constants.ENABLE_ALPHA);
    continueButtonIsClickable.set(true);
    errorLabelVisibility.set(View.GONE);
  }

  private void disableContinueButton() {
    continueButtonVisibility.set(Constants.DISABLE_ALPHA);
    continueButtonIsClickable.set(false);
    errorLabelVisibility.set(View.VISIBLE);
  }

  SingleLiveEvent<Boolean> getHasToCallTechListActivity() {
    return hasToCallTechListActivity;
  }
}
