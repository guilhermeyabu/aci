package yabu.inatel.br.nicechat.data.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import yabu.inatel.br.nicechat.data.model.Topic;

/**
 * Dao to handle topic operations in room database.
 */
@Dao
public interface TopicDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Topic topic);

    @Update
    void update(Topic topic);

    @Query("SELECT * FROM topic")
    LiveData<List<Topic>> getTopics();
}
