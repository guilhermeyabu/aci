package yabu.inatel.br.nicechat.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Topic entity to store information about topic and if is subscribed.
 */
@Entity(tableName = "topic")
public class Topic implements Serializable {

  @NonNull
  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id")
  private long mId;

  @NonNull
  @ColumnInfo(name = "topic_name")
  private String mTopicName;

  @NonNull
  @ColumnInfo(name = "subscribed")
  private Boolean mIsSubscribed;

  @NonNull
  public long getId() {
    return mId;
  }

  public void setId(@NonNull long mId) {
    this.mId = mId;
  }

  @NonNull
  public String getTopicName() {
    return mTopicName;
  }

  public void setTopicName(@NonNull String mTopicName) {
    this.mTopicName = mTopicName;
  }

  @NonNull
  public Boolean isSubscribed() {
    return mIsSubscribed;
  }

  public void setIsSubscribed(@NonNull Boolean mIsSubscribed) {
    this.mIsSubscribed = mIsSubscribed;
  }
}
