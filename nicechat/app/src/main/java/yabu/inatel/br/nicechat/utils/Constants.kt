package yabu.inatel.br.nicechat.utils

/**
 * Constants class.
 */
class Constants {

    companion object {
        private const val JAVA_TOPIC = "Java"
        private const val NODE_TOPIC = "NodeJS"
        private const val C_TOPIC = "C"
        private const val ANGULAR_TOPIC = "AngularJS"
        private const val ANDROID_TOPIC = "Android"
        private const val IOS_TOPIC = "iOS"
        private const val REST_TOPIC = "REST"
        private const val JAVASCRIPT_TOPIC = "JavaScript"
        const val ADMIN_SENDER = "Admin"

        const val ROOM_NAME_HEADER = "room_name"
        const val NAME_HEADER = "name"
        const val MESSAGE_HEADER = "message"
        const val SEEN_BY_HEADER = "seenBy"
        const val DISABLE_ALPHA = 0.5f
        const val ENABLE_ALPHA = 1.0f
        const val EMPTY_FIELD = ""

        @JvmField
        val topicList = arrayListOf(JAVA_TOPIC, NODE_TOPIC, C_TOPIC, ANGULAR_TOPIC, ANDROID_TOPIC,
                IOS_TOPIC, REST_TOPIC, JAVASCRIPT_TOPIC)
    }
}