package yabu.inatel.br.nicechat.ui.chatroom;

import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.ChatApplication;
import yabu.inatel.br.nicechat.data.model.Message;
import yabu.inatel.br.nicechat.databinding.ActivityChatRoomBinding;
import yabu.inatel.br.nicechat.utils.Constants;
import yabu.inatel.br.nicechat.utils.Logger;
import yabu.inatel.br.nicechat.utils.SingleLiveEvent;

/**
 * ViewModel to {@link ChatRoomActivity} to handle operations related to conversation
 * in a group.
 */
public class ChatRoomViewModel {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * Reference to {@link FirebaseDatabase} to get chat information.
   */
  private DatabaseReference mDatabaseReference;

  /**
   * Controls the send button {@link android.widget.Button}  clickable action.
   */
  public final ObservableField<Boolean> sendButtonIsClickable;

  /**
   * Controls the send button {@link android.widget.Button} visibility/alpha
   */
  public final ObservableField<Float> sendButtonVisibility;

  /**
   * Store message to be send to the chat room.
   */
  public final ObservableField<String> message;

  /**
   * Stores all messages in a chat.
   */
  private List<Message> mMessageList;

  /**
   * Binding variable to control layout operations.
   */
  private ActivityChatRoomBinding mBinding;

  /**
   * Indicates that loading has finished.
   */
  private SingleLiveEvent<Boolean> loadingHasFinished;

  /**
   * Adapter to build correctly the chat room.
   */
  @Inject
  ChatRoomAdapter chatRoomAdapter;

  @Inject
  public ChatRoomViewModel() {
    sendButtonIsClickable = new ObservableField<>(false);
    sendButtonVisibility = new ObservableField<>(Constants.DISABLE_ALPHA);
    message = new ObservableField<>(Constants.EMPTY_FIELD);
    mMessageList = new ArrayList<>();
    loadingHasFinished = new SingleLiveEvent<>();
  }

  /**
   * Defines room name to get all messages from {@link FirebaseDatabase}.
   *
   * @param roomName - current chat room.
   */
  void setRoomName(String roomName) {
    if (Logger.DEBUG) Log.d(TAG, "setRoomName: " + roomName);
    mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(roomName);
  }

  /**
   * Set binding variable from activity to update chat recycler view.
   *
   * @param binding - activity binding variable.
   */
  void setBinding(ActivityChatRoomBinding binding) {
    mBinding = binding;
  }

  /**
   * Reflects Activity onResume lifecycle and setup message list.
   */
  void onResume() {
    if (Logger.DEBUG) Log.d(TAG, "onResume");

    mMessageList.clear();
    addDatabaseListener();
  }

  private void addDatabaseListener() {
    mDatabaseReference.addChildEventListener(new ChildEventListener() {
      @Override
      public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        if (Logger.DEBUG) Log.d(TAG, "onChildAdded");
        addMessageToChat(dataSnapshot);
      }

      @Override
      public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        if (Logger.DEBUG) Log.d(TAG, "onChildChanged");
      }

      @Override
      public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

      }

      @Override
      public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {

      }
    });
  }

  /**
   * Sends new message to current group.
   *
   * @return - new View.OnClickListener.
   */
  public View.OnClickListener sendAction() {
    return view -> sendNewMessage();
  }

  private void addMessageToChat(DataSnapshot dataSnapshot) {
    if (Logger.DEBUG) Log.d(TAG, "addMessageToChat");

    Iterator iterator = dataSnapshot.getChildren().iterator();
    String key = dataSnapshot.getKey();
    Message message;

    while (iterator.hasNext()) {
      message = new Message();
      message.setMessage((String) ((DataSnapshot) iterator.next()).getValue());
      message.setOwner((String) ((DataSnapshot) iterator.next()).getValue());

      defineReadStatus(key, message, iterator.next());
    }

    setData(mMessageList);
    mBinding.rvChat.scrollToPosition(mMessageList.size() - 1);
    loadingHasFinished.setValue(true);
  }

  /**
   * Checks if current user has read the current message. If not, add the user to the read list.
   *
   * @param key     -  message key in {@link FirebaseDatabase}.
   * @param message - current {@link Message}.
   * @param next    - next database object to iterate.
   */
  private void defineReadStatus(String key, Message message, Object next) {
    if (key != null) {
      DatabaseReference messageReference = mDatabaseReference.child(key);
      DatabaseReference seenByRef = messageReference.child(Constants.SEEN_BY_HEADER);

      DataSnapshot seenBySnapShot = (DataSnapshot) next;
      Iterator seenByIterator = seenBySnapShot.getChildren().iterator();

      String userThatRead;
      Boolean hasRead = false;

      while (seenByIterator.hasNext()) {
        userThatRead = (String) ((DataSnapshot) seenByIterator.next()).getValue();

        if (Objects.requireNonNull(userThatRead).equals(ChatApplication.prefs.getUserName())) {
          hasRead = true;
        }
      }

      if (!hasRead) {
        confirmReadMessage(seenByRef);
      }

      message.setStatus(seenBySnapShot.getChildrenCount() - 1);
      mMessageList.add(message);
    }
  }

  private void setData(List<Message> messageList) {
    if (Logger.DEBUG) Log.d(TAG, "setData: " + messageList.size());
    chatRoomAdapter.repopulate(messageList);
  }

  private void sendNewMessage() {
    Map<String, Object> map = new HashMap<>();

    //Creates new unique random key for message
    String temp_key = mDatabaseReference.push().getKey();
    mDatabaseReference.updateChildren(map);

    if (temp_key != null) {
      //create new message inside the new path created if temp_key
      DatabaseReference messageReference = mDatabaseReference.child(temp_key);
      Map<String, Object> messageMap = new HashMap<>();
      messageMap.put(Constants.NAME_HEADER, ChatApplication.prefs.getUserName());
      messageMap.put(Constants.MESSAGE_HEADER, message.get());
      messageMap.put(Constants.SEEN_BY_HEADER, Constants.EMPTY_FIELD);
      messageReference.updateChildren(messageMap);

      DatabaseReference seenByRef = messageReference.child(Constants.SEEN_BY_HEADER);
      confirmReadMessage(seenByRef);

      addReadReceipt(messageReference);
    }

    message.set(Constants.EMPTY_FIELD);
  }

  private void confirmReadMessage(DatabaseReference seenByRef) {
    Map<String, Object> seenMap = new HashMap<>();
    seenMap.put("name" + ChatApplication.prefs.getUserName(), ChatApplication.prefs.getUserName());
    seenByRef.updateChildren(seenMap);
  }

  private void addReadReceipt(DatabaseReference messageReference) {
    DatabaseReference seenByReference = messageReference.child(Constants.SEEN_BY_HEADER);

    ValueEventListener eventListener = new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        long seenBy = dataSnapshot.getChildrenCount();
        Log.d(TAG, "Seen by: " + seenBy);
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
      }
    };
    seenByReference.addListenerForSingleValueEvent(eventListener);
  }

  /**
   * Check if message that will be send to the chat is ok.
   *
   * @param message - message that will be send to chat.
   */
  void verifyMessage(String message) {
    if (message.length() > 0) {
      enableSendButton();
    } else {
      disableSendButton();
    }
  }

  private void enableSendButton() {
    sendButtonVisibility.set(Constants.ENABLE_ALPHA);
    sendButtonIsClickable.set(true);
  }

  private void disableSendButton() {
    sendButtonVisibility.set(Constants.DISABLE_ALPHA);
    sendButtonIsClickable.set(false);
  }

  public SingleLiveEvent<Boolean> getLoadingHasFinished() {
    return loadingHasFinished;
  }
}
