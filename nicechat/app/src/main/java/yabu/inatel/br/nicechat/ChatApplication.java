package yabu.inatel.br.nicechat;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import yabu.inatel.br.nicechat.di.Injector;
import yabu.inatel.br.nicechat.utils.Logger;
import yabu.inatel.br.nicechat.utils.Prefs;

/**
 * Application class.
 */
public class ChatApplication extends Application {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * {@link Prefs} instance to allow access in all the application.
   */
  public static Prefs prefs;

  @Override
  public void onCreate() {
    super.onCreate();
    if (Logger.DEBUG) Log.d(TAG, "onCreate");
    prefs = new Prefs(getApplicationContext());
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    if (Logger.DEBUG) Log.d(TAG, "NiceChat app started, version: " + getPackageInfo().versionName);
    Injector.initializeApplicationComponent(this);
    Injector.getApplicationComponent().inject(this);

  }

  private PackageInfo getPackageInfo() {
    if (Logger.DEBUG) Log.d(TAG, "getPackageInfo");
    final PackageManager packageManager = getPackageManager();
    try {
      return packageManager.getPackageInfo(getPackageName(), 0);
    } catch (PackageManager.NameNotFoundException e) {
      throw new RuntimeException("Could not find package info of " + getPackageName());
    }
  }
}
