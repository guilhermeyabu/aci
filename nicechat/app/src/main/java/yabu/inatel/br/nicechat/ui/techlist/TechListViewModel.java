package yabu.inatel.br.nicechat.ui.techlist;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MediatorLiveData;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.data.model.Topic;
import yabu.inatel.br.nicechat.ui.AppController;
import yabu.inatel.br.nicechat.utils.Logger;
import yabu.inatel.br.nicechat.utils.SingleLiveEvent;

/**
 * ViewModel to {@link TechListActivity} to handle operations.
 */
public class TechListViewModel {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * List with topics that user can subscribe.
   */
  private List<Topic> mTopicList;

  /**
   * Activity's Lifecycle.
   */
  private LifecycleOwner mLifecycleOwner;

  /**
   * Adapter to build list of {@link Topic}.
   */
  @Inject
  TechListAdapter techListAdapter;

  /**
   * Controller to handle {@link android.arch.persistence.room.RoomDatabase} operations.
   */
  @Inject
  AppController appController;

  @Inject
  public TechListViewModel() {
    mTopicList = new ArrayList<>();
  }

  /**
   * Set {@link TechListActivity} lifecycle to observe {@link android.arch.lifecycle.LiveData}.
   *
   * @param lifecycleOwner - Activity Lifecycle.
   */
  void setLifecycleOwner(LifecycleOwner lifecycleOwner) {
    mLifecycleOwner = lifecycleOwner;
  }

  /**
   * Reflects Activity onResume lifecycle.
   */
  void onResume() {
    if (Logger.DEBUG) Log.d(TAG, "onResume");

    final MediatorLiveData<List<Topic>> topicListObservable = appController.getTopics();

    topicListObservable.observe(mLifecycleOwner, topicsList -> {
      if (topicsList.size() > 0) {
        if (Logger.DEBUG) Log.d(TAG, "topics not null");
        mTopicList = topicsList;
        setData(mTopicList);
        topicListObservable.removeObservers(mLifecycleOwner);
      }
    });
  }

  private void setData(List<Topic> topicList) {
    if (Logger.DEBUG) Log.d(TAG, "setData: " + topicList.size());
    techListAdapter.repopulate(topicList);
  }

  SingleLiveEvent<Intent> getIntentToChat() {
    return techListAdapter.getIntentToChat();
  }
}
