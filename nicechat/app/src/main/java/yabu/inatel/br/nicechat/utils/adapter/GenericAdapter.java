package yabu.inatel.br.nicechat.utils.adapter;

import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import yabu.inatel.br.nicechat.utils.Logger;

/**
 * Generic Adapter that bind a layout with recycler view.
 *
 * @param <T>  - Type of class in a Recycler View and List.
 * @param <VH> - View Holder type {@link BindingHolder}.
 */
public abstract class GenericAdapter<T, VH extends RecyclerView.ViewHolder> extends
    RecyclerView.Adapter<VH> {

  /**
   * Tag to control Logs.
   */
  public static final String TAG = Logger.getTag();

  /**
   * List of a object of {@link T} defined type.
   */
  private List<T> mList;

  public GenericAdapter() {
    mList = new ArrayList<>();
  }

  @Override
  public int getItemCount() {
    return mList.size();
  }

  /**
   * Reset of Dataset in RecyclerView.
   *
   * @param items Collection of items.
   */
  @UiThread
  public void repopulate(Collection<T> items) {
    clear();
    addAll(items);
    notifyDataSetChanged();
  }

  /**
   * Returns a item from List according to position}.
   * @param position - position of item in the list.
   * @return - Object of {@link T} type in the position.
   */
  protected T getItem(int position) {
    return mList.get(position);
  }

  /**
   * Add a Collection of the items in Dataset of the RecyclerView.
   *
   * @param items Collection of items.
   */
  private void addAll(Collection<T> items) {
    mList.addAll(items);
  }

  /**
   * Clear all items of the Dataset of the RecyclerView.
   */
  private void clear() {
    mList.clear();
  }
}
