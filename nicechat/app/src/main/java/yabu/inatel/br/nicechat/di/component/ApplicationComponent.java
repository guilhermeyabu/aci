package yabu.inatel.br.nicechat.di.component;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.annotations.NonNull;
import yabu.inatel.br.nicechat.ChatApplication;
import yabu.inatel.br.nicechat.di.modules.ApplicationModule;
import yabu.inatel.br.nicechat.di.modules.RoomModule;
import yabu.inatel.br.nicechat.ui.chatroom.ChatRoomActivity;
import yabu.inatel.br.nicechat.ui.nickname.NicknameActivity;
import yabu.inatel.br.nicechat.ui.splashscreen.SplashScreenActivity;
import yabu.inatel.br.nicechat.ui.techlist.TechListActivity;

/**
 * Component to define modules to be inject in each defined class.
 */
@Singleton
@Component(modules = {
    ApplicationModule.class,
    RoomModule.class
})
public interface ApplicationComponent {
  void inject(@NonNull ChatApplication application);

  void inject(@NonNull NicknameActivity nicknameActivity);

  void inject(@NonNull SplashScreenActivity splashScreenActivity);

  void inject(@NonNull TechListActivity techListActivity);

  void inject(@NonNull ChatRoomActivity chatRoomActivity);
}
