package yabu.inatel.br.nicechat.ui;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.util.Log;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import yabu.inatel.br.nicechat.data.model.Topic;
import yabu.inatel.br.nicechat.data.room.ApplicationDatabase;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * Handle the communication between app (View Model) and the RoomDatabase.
 */
@Singleton
public class AppController {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * Application Database Instance.
   */
  private static ApplicationDatabase mApplicationDatabase;

  /**
   * Application instance.
   */
  @Inject
  Application application;

  @Inject
  public AppController(ApplicationDatabase applicationDatabase) {
    mApplicationDatabase = applicationDatabase;
  }

  /**
   * Create a new {@link Topic} in the database.
   *
   * @param topic - new topic to be created.
   */
  public void createNewTopic(Topic topic) {
    if (Logger.DEBUG) Log.d(TAG, "createNewTopic: " + topic.getTopicName());

    Executor myExecutor = Executors.newSingleThreadExecutor();
    myExecutor.execute(() -> mApplicationDatabase.topicDao().insert(topic));
  }

  /**
   * Updates the current topic in the database.
   *
   * @param topic - topic to be updated.
   */
  public void updateTopic(Topic topic) {
    if (Logger.DEBUG) Log.d(TAG, "updateTopic: " + topic.getTopicName());

    Executor myExecutor = Executors.newSingleThreadExecutor();
    myExecutor.execute(() -> mApplicationDatabase.topicDao().update(topic));
  }

  /**
   * Gets list of topics stored in the {@link android.arch.persistence.room.RoomDatabase}.
   *
   * @return - List of Topics in a {@link MediatorLiveData}.
   */
  public MediatorLiveData<List<Topic>> getTopics() {
    if (Logger.DEBUG) Log.d(TAG, "getTopics");

    MediatorLiveData<List<Topic>> mediatorLiveData = new MediatorLiveData<>();
    LiveData<List<Topic>> userLiveData = mApplicationDatabase.topicDao().getTopics();
    mediatorLiveData.addSource(userLiveData, mediatorLiveData::setValue);

    return mediatorLiveData;
  }
}
