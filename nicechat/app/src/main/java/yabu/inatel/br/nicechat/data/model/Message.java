package yabu.inatel.br.nicechat.data.model;

/**
 * Message model class to store chat information.
 */
public class Message {

  private String mMessage;

  private String mOwner;

  private Long mStatus;

  public String getMessage() {
    return mMessage;
  }

  public void setMessage(String message) {
    mMessage = message;
  }

  public String getOwner() {
    return mOwner;
  }

  public void setOwner(String owner) {
    mOwner = owner;
  }

  public Long getStatus() {
    return mStatus;
  }

  public void setStatus(Long status) {
    mStatus = status;
  }
}
