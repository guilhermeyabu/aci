package yabu.inatel.br.nicechat.ui.techlist;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.R;
import yabu.inatel.br.nicechat.databinding.ActivityTechListBinding;
import yabu.inatel.br.nicechat.di.Injector;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * Tech List Activity to allow user to subscribe to a topic and chat with anothers users.
 */
public class TechListActivity extends AppCompatActivity {

  /**
   * Tag to control Logs.
   */
  public static final String TAG = Logger.getTag();

  /**
   * Binding variable to control layout operations.
   */
  private ActivityTechListBinding mBinding;

  @Inject
  TechListViewModel viewModel;

  /**
   * Starts the {@link TechListActivity}.
   *
   * @param context - context of the previous Activity.
   */
  public static void startActivity(Context context) {
    if (Logger.DEBUG) Log.d(TAG, "startActivity");
    Intent intent = new Intent(context, TechListActivity.class)
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(intent);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (Logger.DEBUG) Log.d(TAG, "onCreate");

    Injector.getApplicationComponent().inject(this);
    mBinding = DataBindingUtil.setContentView(this, R.layout.activity_tech_list);
    mBinding.rvTopicList.setAdapter(viewModel.techListAdapter);
    viewModel.setLifecycleOwner(this);

    viewModel.getIntentToChat().observe(this, this::startActivity);
  }

  @Override
  protected void onResume() {
    super.onResume();
    viewModel.onResume();
  }
}
