package yabu.inatel.br.nicechat.ui.chatroom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import yabu.inatel.br.nicechat.ChatApplication;
import yabu.inatel.br.nicechat.R;
import yabu.inatel.br.nicechat.data.model.Message;
import yabu.inatel.br.nicechat.databinding.RowChatBinding;
import yabu.inatel.br.nicechat.utils.Logger;
import yabu.inatel.br.nicechat.utils.adapter.BindingHolder;
import yabu.inatel.br.nicechat.utils.adapter.GenericAdapter;

/**
 * Adapter to {@link ChatRoomActivity} to build correctly the conversation.
 */
public class ChatRoomAdapter extends GenericAdapter<Message, BindingHolder<RowChatBinding>> {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * Context to get resources.
   */
  @Inject
  Context mContext;

  @Inject
  public ChatRoomAdapter() {
    super();
  }

  @NonNull
  @Override
  public BindingHolder<RowChatBinding> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    if (Logger.DEBUG) Log.d(TAG, "onCreateViewHolder");
    return new BindingHolder<>(mContext, viewGroup, R.layout.row_chat);
  }

  @Override
  public void onBindViewHolder(@NonNull BindingHolder<RowChatBinding> holder, int position) {
    if (Logger.DEBUG) Log.d(TAG, "onBindViewHolder");

    Message message = getItem(position);
    RowChatBinding row = holder.binding;

    if (row != null) {
      setupMessage(row, message);
    }

    holder.setIsRecyclable(false);
  }

  private void setupMessage(RowChatBinding binding, Message message) {
    if (message.getOwner().equals(ChatApplication.prefs.getUserName())) {
      setupOwner(binding, message);
    } else {
      setupPeopleMessage(binding, message);
    }
  }

  private void setupOwner(RowChatBinding binding, Message message) {
    binding.tvChatterName.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
    binding.tvMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
    binding.tvMessageStatus.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
    binding.clChat.setBackground(ContextCompat.getDrawable(mContext, R.drawable.chat_owner));
    binding.tvChatterName.setText(message.getOwner());
    binding.tvMessage.setText(message.getMessage());

    if (message.getStatus() > 0) {
      binding.tvMessageStatus.setText(
          String.format(mContext.getString(R.string.seen_by), message.getStatus()));
    } else {
      binding.tvMessageStatus.setText(mContext.getString(R.string.message_send));
    }
  }

  private void setupPeopleMessage(RowChatBinding binding, Message message) {
    binding.tvChatterName.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
    binding.tvMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
    binding.tvMessageStatus.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
    binding.tvMessage.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
    binding.tvChatterName.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
    binding.clChat.setBackground(ContextCompat.getDrawable(mContext, R.drawable.chat_people));
    binding.tvMessageStatus.setVisibility(View.GONE);
    binding.tvChatterName.setText(message.getOwner());
    binding.tvMessage.setText(message.getMessage());
  }
}
