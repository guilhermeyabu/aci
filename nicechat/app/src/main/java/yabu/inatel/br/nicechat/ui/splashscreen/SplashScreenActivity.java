package yabu.inatel.br.nicechat.ui.splashscreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.FirebaseApp;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import yabu.inatel.br.nicechat.R;
import yabu.inatel.br.nicechat.di.Injector;
import yabu.inatel.br.nicechat.utils.Logger;

/**
 * Splash screen to check if app has a registered user and define next screen.
 */
public class SplashScreenActivity extends AppCompatActivity {

  /**
   * Tag to control Logs.
   */
  private static final String TAG = Logger.getTag();

  /**
   * {@link SplashScreenActivity} timeout value.
   */
  private static final int SPLASH_TIMEOUT = 1;

  /**
   * Disposable to execute a {@link Consumer}.
   */
  private Disposable mSubscription;

  /**
   * ViewModel to handle operations.
   */
  @Inject
  SplashScreenViewModel viewModel;

  /**
   * Consumer to check user registration and calls the correct activity.
   */
  private Consumer mConsumer = o -> {
    viewModel.checkUserRegistration(this);
    finish();
  };

  @Override
  protected void onCreate(@Nullable final Bundle savedInstanceState) {
    if (Logger.DEBUG) Log.d(TAG, "onCreate");
    super.onCreate(savedInstanceState);
    Injector.getApplicationComponent().inject(this);
    FirebaseApp.initializeApp(this);
    setContentView(R.layout.activity_splash_screen);

    viewModel.setLifecycleOwner(this);
    viewModel.initializeTopics();
  }

  @Override
  protected void onResume() {
    if (Logger.DEBUG) Log.d(TAG, "onResume");
    super.onResume();
    mSubscription = Observable.timer(SPLASH_TIMEOUT, TimeUnit.SECONDS).subscribe(mConsumer);
  }

  @Override
  protected void onPause() {
    super.onPause();
    mSubscription.dispose();
  }
}
